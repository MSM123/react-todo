import React from 'react';
class Counter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0
    };

    this.incCount = this.incCount.bind(this);
    this.decCount = this.decCount.bind(this);
    this.resetCount = this.resetCount.bind(this);
    this.stopCount = this.stopCount.bind(this);
  }

  incCount() {
    setInterval(() => {
      this.setState({
        count: this.state.count + 1
      });
    }, 100);
  }

  decCount() {
    setInterval(() => {
      this.setState({
        count: this.state.count - 1
      });
    }, 100);
  }

  resetCount() {
    this.setState({
      count: 0
    });
  }

  stopCount() {
    clearInterval(
      this.setState({
        count: this.state.count
      })
    );
  }

  render() {
    return (
      <div style={{ display: 'flex' }}>
        <button onClick={this.incCount}>+</button>
        <h1>{this.state.count}</h1>
        <button onClick={this.decCount}>-</button>
        <button onClick={this.resetCount} style={{ marginLeft: '5px' }}>
          Reset
        </button>
        <button onClick={this.stopCount} style={{ marginLeft: '5px' }}>
          Stop
        </button>
      </div>
    );
  }
}

export default Counter;
