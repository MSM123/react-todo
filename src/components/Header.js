import React from 'react';
import { Link } from 'react-router-dom';

class Header extends React.Component {
  render() {
    return (
      <header style={headerStyle}>
        <h1>TodoList</h1>
        <Link to="/" style={linkStyle}>
          Home
        </Link>{' '}
        |{' '}
        <Link to="/about" style={linkStyle}>
          About
        </Link>
      </header>
    );
  }
}

const headerStyle = {
  textAlign: 'center',
  backgroundColor: '#0000A0',
  padding: '10px',
  color: '#ff0000'
};
const linkStyle = {
  color: '#fff',
  textDecoration: 'none'
};
export default Header;
