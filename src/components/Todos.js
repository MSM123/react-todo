import React from 'react';
import * as uuid from 'uuid';
import Todoinput from '../components/Todoinput';
import Todolist from '../components/Todolist';

class Todos extends React.Component {
  constructor() {
    super();
    this.state = {
      items: [],
      item: '',
      id: 0,
      completed: false
    };
  }
  handleChange = e => {
    this.setState({
      item: e.target.value
    });
  };
  handleSubmit = e => {
    e.preventDefault();
    if (this.state.item === '') {
      alert('please enter some text');
    }
    const newItem = {
      id: this.state.id,
      item: this.state.item,
      completed: this.state.completed
    };
    const updatedItems = [...this.state.items, newItem];
    this.setState({
      items: updatedItems,
      item: '',
      id: uuid.v4(),
      completed: false
    });
  };

  handleDelete = id => {
    const filteredItems = this.state.items.filter(data => data.id !== id);
    this.setState({
      items: filteredItems
    });
  };

  markComplete = id => {
    this.setState({
      items: this.state.items.map(data => {
        if (data.id === id) {
          data.completed = !data.completed;
        }
        return data;
      })
    });
  };

  render() {
    return (
      <div className="container ">
        <Todoinput
          item={this.state.item}
          handleChange={this.handleChange}
          handleSubmit={this.handleSubmit}
        />
        <Todolist
          items={this.state.items}
          handleDelete={this.handleDelete}
          markComplete={this.markComplete}
        />
      </div>
    );
  }
}

export default Todos;
