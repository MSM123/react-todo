import React from 'react';

class Todoinput extends React.Component {
  render() {
    const { item, handleChange, handleSubmit } = this.props;
    return (
      <div className="container bg-dark">
        <form className="d-flex p-2" onSubmit={handleSubmit}>
          <div className="container d-flex justify-content-between">
            <input
              type="text"
              placeholder="Enter here"
              value={item}
              onChange={handleChange}
            />
            <button className="btn btn-primary" type="submit">
              Add Todo Item
            </button>
          </div>
        </form>
      </div>
    );
  }
}

export default Todoinput;
