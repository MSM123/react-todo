import React from 'react';

class Todolist extends React.Component {
  render() {
    const { items, handleDelete, markComplete } = this.props;
    const completedStyle = {
      textDecoration: 'line-through',
      color: '#cdcdcd'
    };

    return (
      <ul className="list-group">
        {items.map(data => {
          if (data.item !== '') {
            var li = (
              <li
                className="list-group-item bg-primary text-white d-flex justify-content-between"
                key={data.id}
              >
                <div style={data.completed ? completedStyle : null}>
                  <input
                    type="checkbox"
                    checked={data.completed}
                    onChange={() => markComplete(data.id)}
                  />{' '}
                  {data.item}
                </div>
                <button
                  className="btn btn-danger"
                  onClick={() => handleDelete(data.id)}
                >
                  Delete
                </button>
              </li>
            );
          }
          return li;
        })}
      </ul>
    );
  }
}
export default Todolist;
