import React from 'react';
import './App.css';
import Todos from './components/Todos';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './components/Header';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import About from './components/About';

function App() {
  return (
    <Router>
    <div className="App">
      <Header />
      <Route exact path='/' render={props=> (
        <React.Fragment>
       <Todos />
        </React.Fragment>
      )}/>
      <Route path='/about' component={About} />
    </div>
    </Router>
  );
  }

export default App;
